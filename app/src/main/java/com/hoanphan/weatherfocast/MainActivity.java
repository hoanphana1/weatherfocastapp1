package com.hoanphan.weatherfocast;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.app.VoiceInteractor;
import android.content.Context;
import android.content.Intent;
import android.net.sip.SipSession;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private EditText edtSearch;
    private Button btnSearch,btnChangeActivity;
    private TextView txtName, txtCountry, txtTemp, txtStatus, txtHumidity, txtUV, txtWind, txtDay;
    private ImageView imgIcon;
    String City = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Init();
        GetCurrentWeatherData("Saigon");
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String city = edtSearch.getText().toString();
                if (city.equals("")){
                    City = "Saigon";
                    GetCurrentWeatherData(City);
            }
                else{
                    City = city;
                    GetCurrentWeatherData(City);
                }
            }
        });
        btnChangeActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String city = edtSearch.getText().toString();
                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                intent.putExtra("name",city);
                startActivity(intent);
            }
        });
    }
    public void GetCurrentWeatherData(final String data){
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
 //       String url = "http://api.openweathermap.org/data/2.5/weather?q="+data+"&units=metric&appid=592e1a0160f84c86a226c535f7f423bb";
        String url = "https://api.weatherbit.io/v2.0/current?key=f3897477d7e048918059965407d46ac4&city="+data;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArraydata = jsonObject.getJSONArray("data");
                            JSONObject jsonObjectData = jsonArraydata.getJSONObject(0);
                            String name = jsonObjectData.getString("city_name");
                            txtName.setText("Tên thành phố: "+name);

                            String day = jsonObjectData.getString("datetime");
                            txtDay.setText(day);

                            String country = jsonObjectData.getString("country_code");
                            txtCountry.setText("Tên quốc gia: "+country);

                            String nhietdo = jsonObjectData.getString("temp");
                            String doAm = jsonObjectData.getString("rh");

                            Double a = Double.valueOf(nhietdo);
                            String Nhietdo = String.valueOf(a.intValue());
                            txtTemp.setText(Nhietdo+" °C");
                            txtHumidity.setText(doAm+"%");

                            String gio = jsonObjectData.getString("wind_spd");
                            txtWind.setText(gio+"m/s");

                            String uv = jsonObjectData.getString("uv");
                            txtUV.setText(uv);

                            JSONObject jsonObjectWeather = jsonObjectData.getJSONObject("weather");
                            String status = jsonObjectWeather.getString("description");
                            String icon = jsonObjectWeather.getString("icon");
                            //dung thu vien picasso doc thu vien hinh anh
                            Picasso.with(MainActivity.this).load("https://www.weatherbit.io/static/img/icons/"+icon+".png").into(imgIcon);
                            txtStatus.setText(status);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue.add(stringRequest);
    }
    private void Init() {
        edtSearch = findViewById(R.id.edt_Search);
        btnSearch = findViewById(R.id.btn_Search);
        btnChangeActivity = findViewById(R.id.btn_NextDay);
        txtName = findViewById(R.id.Tv_Name);
        txtCountry =findViewById(R.id.Tv_Country);
        txtTemp = findViewById(R.id.Tv_Temp);
        txtStatus = findViewById(R.id.Tv_Status);
        txtDay = findViewById(R.id.Tv_Day);
        txtHumidity = findViewById(R.id.Tv_Humidity);
        txtUV = findViewById(R.id.Tv_UV);
        txtWind = findViewById(R.id.Tv_Wind);
        imgIcon = findViewById(R.id.Iv_Icon);
    }
}