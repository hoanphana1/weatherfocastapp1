package com.hoanphan.weatherfocast;

import androidx.annotation.NonNull;

public class Weather {
    public static String Day;
    public static String Status;
    public static String Image;
    public static String MaxTemp;
    public static String MinTemp;

    public Weather(String day, String status, String image, String maxTemp, String minTemp) {
        Day = day;
        Status = status;
        Image = image;
        MaxTemp = maxTemp;
        MinTemp = minTemp;
    }

    @NonNull
    @Override
    public String toString() {
        return Day + Status + Image + MaxTemp + MinTemp;
    }
}
