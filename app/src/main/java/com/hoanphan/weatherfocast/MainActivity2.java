package com.hoanphan.weatherfocast;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity2 extends AppCompatActivity {

    String tenthanhpho = "";
    ImageView imgback;
    TextView tvName;
    ListView lv;
    CustomAdapter customAdapter;
    ArrayList<Weather> arrayWeather;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        Init();
        String city = intent.getStringExtra("name");
        if(city.equals("")){
            tenthanhpho = "Saigon";
            Get16daydata(tenthanhpho);
        }else{
            tenthanhpho=city;
            Get16daydata(tenthanhpho);
        }
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void Init() {
        imgback = findViewById(R.id.imageviewBack);
        tvName = findViewById(R.id.tvTenTp);
        lv = findViewById(R.id.listView);
        arrayWeather = new ArrayList<>();
        customAdapter = new CustomAdapter(MainActivity2.this,arrayWeather);
        lv.setAdapter(customAdapter);
    }

    private void Get16daydata(String data) {
        String url = "https://api.weatherbit.io/v2.0/forecast/daily?key=f3897477d7e048918059965407d46ac4&city="+data;
        //sử dụng thư viện Volley để tạo request tới server, xử lý kết quả trả về là JSON.
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity2.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String name = jsonObject.getString( "city_name");
                            tvName.setText(name);

                            JSONArray jsonArrayList = jsonObject.getJSONArray("data");
                            Log.d("16days", "onResponse: " + jsonArrayList.length());
                            for (int i=0; i<jsonArrayList.length(); i++){
                                JSONObject jsonObjectList = jsonArrayList.getJSONObject(i);

                                String ngay = jsonObjectList.getString("datetime");
                                String max = jsonObjectList.getString("max_temp");
                                String min = jsonObjectList.getString("low_temp");


                                Double a = Double.valueOf(max);
                                String NhietdoMax = String.valueOf(a.intValue());
                                Double b = Double.valueOf(min);
                                String NhietdoMin = String.valueOf(b.intValue());

                                JSONObject jsonObjectWeather = jsonObjectList.getJSONObject("weather");//JsonArray dc truy cap bang index
                                String status = jsonObjectWeather.getString("description");
                                String icon = jsonObjectWeather.getString("icon");

                                Weather weather = new Weather(ngay,status,icon,NhietdoMax,NhietdoMin);
                                Log.d("weather", weather.toString());
                                arrayWeather.add(weather);
                            }

                            customAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue.add(stringRequest);
    }
}