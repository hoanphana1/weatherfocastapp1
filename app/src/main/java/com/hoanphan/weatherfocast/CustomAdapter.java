package com.hoanphan.weatherfocast;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LongDef;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    Context context;
    ArrayList<Weather> arrayList;

    public CustomAdapter(Context context, ArrayList<Weather> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.item_listview,null);
        Log.d("Adapter", "getView: " + i);
        Weather weather = arrayList.get(i);

        TextView tvDay = view.findViewById(R.id.TvNgay);
        TextView tvStatus = view.findViewById(R.id.TvTrangThai);
        TextView tvMaxTemp = view.findViewById(R.id.tvMaxTemp);
        TextView tvMinTemp = view.findViewById(R.id.tvMinTemp);
        ImageView imgStatus = view.findViewById(R.id.imageviewTrangThai);

        tvDay.setText(weather.Day);
        tvStatus.setText(weather.Status);
        tvMaxTemp.setText(weather.MaxTemp +"°C");
        tvMinTemp.setText(weather.MinTemp +"°C");
        Picasso.with(context).load("https://www.weatherbit.io/static/img/icons/"+ weather.Image +".png").into(imgStatus);

        return view;
    }
}
